<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa',100);
            $table->string('nit',50);
            $table->string('contacto',200);
            $table->string('codigo',10)->unique();
            $table->string('correo',50)->unique();
            $table->string('direccion',100);
            $table->string('celular',20);
            $table->string('telefono',20);
            $table->datetime('fecha_registro');
            $table->decimal('latitud',10,6);
            $table->decimal('longitud',10,6);
            
            $table->integer('id_estado')->unsigned();
            $table->foreign('id_estado')->references('id')->on('estados');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
