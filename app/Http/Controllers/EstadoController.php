<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Estado;

class EstadoController extends Controller
{
    public function index(){
    	$estados = Estado::all();
    	return view('estado.index')->with('estados',$estados);
    }

    public function create(){
    	return view('estado.create'); 
    }

    public function store(Request $request){
    	Estado::create([
    			'id'=>$request['id'],
    			'detalle'=>$request['detalle'],
    		]);
    	return redirect('/estado')->with('message','ok');
    }
//    
//    public function update($id, Request $request){
//		$estado = Estado::find($id);
//		$estado->fill($request->all());
//		$estado->save();
//		Session::flash('message','Estado Editado.');
//		return Redirect::to('/estado');
//    }
//
//    public function edit($id){
//    	$estado = Estado::find($id);
//    	return view('estado.edit',['estado'=>$estado]);
//    }
//
//    public function destroy($id){
//    	Estado::destroy($id);
//    	Session::flash('message','Estado Eliminado.');
//        return Redirect::to('/estado');
//    }
}
