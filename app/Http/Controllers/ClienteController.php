<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Cliente;

class ClienteController extends Controller
{
    public function index(){
    	$clientes = Cliente::all();
    	return view('cliente.index')->with('clientes',$clientes);
    }

    public function create(){
    	return view('cliente.create'); 
    }

    public function store(Request $request){
    	Cliente::create([
    			'id'=>$request['id'],
    			'empresa'=>$request['empresa'],
    			'nit'=>$request['nit'],
                        'contacto'=>$request['contacto'],
                        'codigo'=>$request['codigo'],
                        'correo'=>$request['correo'],
                        'direccion'=>$request['direccion'],
                        'celular'=>$request['celular'],
                        'telefono'=>$request['telefono'],
                        'fecha_registro'=>$request['fecha_registro'],
                        'latitud'=>$request['latitud'],
                        'longitud'=>$request['longitud'],
                        'id_estado'=>$request['id_estado'],
    		]);
    	return redirect('/cliente')->with('message','ok');
    }
}
