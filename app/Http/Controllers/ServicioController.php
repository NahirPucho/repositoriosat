<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Requests;
use App\Servicio;

class ServicioController extends Controller
{
    public function index(){
    	$services = Servicio::all();
    	return view('service.index')->with('services',$services);
    }

    public function create(){
    	return view('service.create'); 
    }

    public function store(Request $request){
    	Servicio::create([
    			'nombre'=>$request['nombre'],
    			'descripcion'=>$request['descripcion'],
    			'precio'=>$request['precio'],
    		]);
    	return redirect('/service')->with('message','ok');
    }

    public function update($id, Request $request){
		$service = Servicio::find($id);
		$service->fill($request->all());
		$service->save();
		Session::flash('message','Servicio Editado.');
		return Redirect::to('/service');
    }

    public function edit($id){
    	$service = Servicio::find($id);
    	return view('service.edit',['service'=>$service]);
    }

    public function destroy($id){
    	Servicio::destroy($id);
    	Session::flash('message','Servicio Eliminado.');
        return Redirect::to('/service');
		
    }
}
