<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cliente;
use App\Servicio;
use Session;
use App\NotaServicio;

session_start();
class NotaServicioController extends Controller
{
    
	public function index(){

	}

	public function create(){

		return view('notas.create');
	}

	public function store(){
		return "correcto";
	}

	public function buscarCliente(Request $request){
		if ($request->ajax()){
			$ciClient=$request['ci'];
			//consultamos si existe
			$objCli=new Cliente();
			$result=$objCli->getCliente($ciClient);
			if (!is_null($result)){
				//cliente existe
				$nombComple=$result->nombres;
				$nombComple.=' '.$result->apellidos;
             	//obtenemos todos los servicios 
				$objServi=new Servicio();
				$colecObjServ=$objServi->getAllService();
             	return response()->json(['nombComple' => $nombComple,'servicios'=>$colecObjServ->toArray()]);
             	//echo $nombComple;   

			}else{
				//echo 'no existe';
             	return response()->json(['nombComple' => 'No existe Cliente']); 
			}

		}else{
			return 'No fue ajax';
		}

	}

	public function agregarServicio(Request $request){
			if($request->ajax()){
			   
			$ciClient=$request['ci'];
			$idServi=$request['idServi'];
			$cantidad=$request['cantidad'];

			//$idServi=2;
			//$cantidad=5;

			$objServ=new Servicio();
			$dataService=$objServ->getServicio($idServi);
			

			if(!is_null($dataService)){
				//guardamos en la session
				if(!isset($_SESSION['arrayServicio'])){
					$arryItem[]=array(
								'idservi'=>$dataService->id,
								'nombreservi'=>$dataService->nombre,
								'precio'=>$dataService->precio,
								'cantidad'=>$cantidad
								);
					//session(['arrayServicio'=>$arryItem]);
					$_SESSION['arrayServicio']=$arryItem;
					$_SESSION['idClient']=$ciClient;
					$total=($dataService->precio * $cantidad);
					$_SESSION['total']=$total;

				
				}else{
					$array=$_SESSION['arrayServicio'];
					$total=0;
					$itemServi=array(
								'idservi'=>$dataService->id,
								'nombreservi'=>$dataService->nombre,
								'precio'=>$dataService->precio,
								'cantidad'=>$cantidad
								);
					array_push($array,$itemServi);					
					//session(['arrayServicio'=>$array]);
					$_SESSION['arrayServicio']=$array;
					foreach ($array as $item) {
						$total+=($item['cantidad']*$item['precio']);
					}
					$_SESSION['total']=$total;					
					//dd($_SESSION['arrayServicio']);
				}
				return response()->json(['arrayServi'=>$_SESSION['arrayServicio'],'total'=>$_SESSION['total']]);

			}else{
				//error
			}
		}

	}

	public function quitarServicio(Request $request){
				session_unset();
				session_destroy();
		return response()->json(['ok'=>'ok']);	

	}

 	public function guardarNota(Request $request){
 		$ci=$_SESSION['idClient'];
 		$total=$_SESSION['total'];
 		$objClient=new Cliente();
 		$resul=$objClient->getCliente($ci);
 		$objNota=new NotaServicio();
 		$objNota->cliente_id=$resul->id;
 		$fechaAct=date('Y-m-d H:i:s');
 		$objNota->fecha=$fechaAct;
 		$objNota->total=$total;
 		$objNota->save();

 		$arreglo=$_SESSION['arrayServicio'];

 		foreach ($arreglo as $value) {
 			$idServi=$value['idservi'];
 			$objNota->servicios()->attach($idServi);
 			
 		}

 		//una ves insertado todo armamos array detalle para el modal
 		

 		//liberamos la session para probar con otro y retornamos 
		session_unset();
		session_destroy();
		return response()->json(['arrayServi'=>$arreglo,'fecha'=>$fechaAct,'total'=>$total,'cliente'=>$resul->toArray()]);	

 	}


    //implementar para cancelar nota matar la session 
    //para luego limpiar los campos 
 	public function cancelarNota(Request $request){

 	}
 	//obtener la session sacar array quitar el elemento con la key
 	//calcular el total guardar y retornar 
 	public function cancelarItemNota(Request $request){

 	}













	public function buscar(){
		$input = Input::all();
		$ci = $input['ci'];
		$clientes = array();
		$cliente = Cliente::where('ci', '=', $ci);
		return View('notas.create',['clientes'=>$cliente]);
	}

	public function show($ci){
		dd($ci);
	}
}
