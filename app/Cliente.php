<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $fillable = ['id','empresa','nit','contacto','codigo','correo','direccion','celular','telefono','fecha_registro','latitud','longitud','id_estado'];
    
    public function Estados(){
    	return $this->hasMany('App\Estado');
    }

    public function getCliente($ci){
    	return Cliente::where('id',$id)->first();

    }
}
