<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';
    protected $fillable = ['nombre','descripcion','precio'];

    public function NotaServicios(){
    	return $this->belongsToMany('App\NotaServicio');
    }


    public function getAllService(){
    	return Servicio::All();
    }

    public function getServicio($idServi){
    	//dd(Servicio::where('id',$idServi)->first()->toArray());
    	return Servicio::where('id',$idServi)->first();
    }
}
