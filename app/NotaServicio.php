<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaServicio extends Model
{
    protected $table = 'nota_servicios';
    protected $fillable = ['fecha','total'];

    public function cliente(){
    	 return $this->belongsTo('App\Cliente');
    }

    public function servicios(){
    	return $this->belongsToMany('App\servicio');
    }


}
