<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estados';
    protected $fillable = ['id','detalle'];
    
//    public function getEstado($id){
//    	return Estado::where('id',$id)->first();
//    }
}
