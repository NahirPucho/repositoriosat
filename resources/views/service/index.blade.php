@extends('layouts.admin')
@if(Session::has('message'))
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	{{Session::get('message')}}
</div>
@endif
@section('content')
<h3>Lista Servicios</h3>	
	<table class="table">
		<thead>
			<th>Nombre</th>
			<th>Descripcion</th>
			<th>Precio</th>
			<th>Accion</th>
		</thead>
		<tbody>
		@foreach($services as $service)
				<td>{{$service->nombre}}</td>
				<td>{{$service->descripcion}}</td>
				<td>{{$service->precio}}</td>
				<td>{!!link_to_route('service.edit', $title = 'Editar', $parameters = $service->id, $attributes = ['class'=>'btn btn-primary'])!!}
				{!!link_to_route('service.destroy', $title = 'Eliminar', $parameters = $service->id, $attributes = ['class'=>'btn btn-danger'])!!}
				</td>
		</tbody>
		@endforeach
	</table>
@endsection


