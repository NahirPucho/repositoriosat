@extends('layouts.admin');
@section('content')
<h3>Editar Servicio</h3>
{!!Form::model($service,['route'=>['service.update', $service->id],'method'=>'PUT'])!!}
	<div class="form-group">
		{!!Form::label('lbl_nombre','Nombre:')!!}
		{!!Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Ingresa el Nombre.'])!!}
	</div>
	<div class="form-group">
		{!!Form::label('lbl_descripcion','Descripcion:')!!}
		{!!Form::textarea('descripcion',null,['class'=>'form-control','placeholder'=>'Ingresa su Descripcion.'])!!}
	</div>
	<div class="form-group">
		{!!Form::label('lbl_precio','Precio:')!!}
		{!!Form::text('precio',null,['class'=>'form-control','placeholder'=>'Ingresa el precio.'])!!}
	</div>
	{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}
@endsection