@extends('layouts.admin');
	@include('modal.detalle')   
	@section('content')




<div id="mensaje"></div>
<h3>Nota Servicio</h3>		
<div class="form-horizontal">
  <div class="form-group">
   
    <label for="ci" class="col-sm-2 control-label">ci</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="ci" name="ci" >
    </div>
    <div class="col-sm-2">
		<button class="btn btn-primary" onclick="buscarCliente()">Buscar</button>      
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Usuario</label>
    <div class="col-sm-4">
		<input type="text" class="form-control " id="nombreCompleto" name="nombreCompleto" disabled="true">	      
    </div>
  </div>


 <div class="form-group">
   
    <label for="servicio" class="col-sm-2 control-label">Servicios</label>
    <div class="col-sm-4">
		<select id="servicio" name="servicio" class="form-control" value="sdfdf">
			<option value="">----------------------------</option>
		<!--aca colocar el otro option con un for-->
		</select>
    </div>
    <div class="col-sm-2">
		<button class="btn btn-success" onclick="agregarServicio()">Agregar</button>    
    </div>
 </div>

</div>
<input type="hidden" class="form-control" id="cantidad" name="cantidad" value="1" >
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">	


	

	<div class="panel panel-primary">
		  <div class="panel-heading">Nota de Venta</div>
		  <div class="panel-body">
		   
			<table  id="tabla-nota" class=" table table-bordered" >
				<thead>
					 <tr>
						 <th>Codigo</th>
						 <th>Servicio</th>
						 <th>Cantidad</th>
						 <th>precio</th>
						</tr>
				 </thead>
								    
					 <tbody>

									
					 </tbody>
						
			</table>
				<div>
					<div style="width:600px;margin:auto;">
					<button class="btn btn-success col-sm-2 control-label" onclick="guardarNotaVenta()">Guardar</button>		
					<button class="btn btn-danger col-sm-2 control-label" onclick="quitarServicio()">cancelar</button>		
					
					</div>
				</div>
	   	

		  </div>
   </div>
	
	
	@endsection