@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-11 col-md-offset-0">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Registrar cliente</h3></div>
            <div class="panel-body">
                {!!Form::open(['route'=>'cliente.store', 'method'=>'POST'])!!}
                <div class="form-group">
                    {!!Form::label('lbl_empresa','Empresa:')!!}
                    {!!Form::text('empresa',null,['class'=>'form-control','placeholder'=>'Ingresa nombre de la empresa'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_nit','NIT:')!!}
                    {!!Form::text('nit',null,['class'=>'form-control','placeholder'=>'Ingresa NIT de empresa'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_contacto','Contacto:')!!}
                    {!!Form::text('contacto',null,['class'=>'form-control','placeholder'=>'Ingrese nombre del contacto'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_codigo','Codigo:')!!}
                    {!!Form::text('codigo',null,['class'=>'form-control','placeholder'=>''])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_correo','Correo:')!!}
                    {!!Form::text('correo',null,['class'=>'form-control','placeholder'=>'Ingrese gmail'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_direccion','Direccion:')!!}
                    {!!Form::text('direccion',null,['class'=>'form-control','placeholder'=>'Direccion de la empresa'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_celular','Cel:')!!}
                    {!!Form::text('celular',null,['class'=>'form-control','placeholder'=>'Numero de contacto'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_telefono','Telf:')!!}
                    {!!Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Telefono de la empresa'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_fecha_registro','Fecha:')!!}
                    {!!Form::text('fecha_registro',null,['class'=>'form-control','placeholder'=>''])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_latitud','Latitud:')!!}
                    {!!Form::text('latitud',null,['class'=>'form-control','placeholder'=>''])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_longitud','Longitud:')!!}
                    {!!Form::text('longitud',null,['class'=>'form-control','placeholder'=>''])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('lbl_id_estado','Estado:')!!}
                    {!!Form::text('id_estado',null,['class'=>'form-control','placeholder'=>''])!!}
                </div>
                {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endsection