@extends('layouts.admin')
<?php $message = Session::get('message') ?>
@if($message == 'ok')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    Cliente Registrado.
</div>
@endif
@section('content')
<div class="row">
    <div class="col-md-11 col-md-offset-0">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Lista de clientes</h3></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <th>ID</th>
                    <th>Empresa</th>
                    <th>NIT</th>
                    <th>Contacto</th>
                    <th>Estado</th>
                    <th>Accion</th>
                    </thead>
                    <tbody>
                        @foreach($clientes as $cliente)
                    <td>{{$cliente->id}}</td>
                    <td>{{$cliente->empresa}}</td>
                    <td>{{$cliente->nit}}</td>
                    <td>{{$cliente->contacto}}</td>
                    <td>{{$cliente->estado}}</td>
                    <td>
                        {!!link_to_route('cliente.edit', $title = 'Editar', $parameters = $cliente->id, $attributes = ['class'=>'btn btn-primary'])!!}
                        {!!link_to_route('cliente.destroy', $title = 'Eliminar', $parameters = $cliente->id, $attributes = ['class'=>'btn btn-danger'])!!}
                    </td>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


