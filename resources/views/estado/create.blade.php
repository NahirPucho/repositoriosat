@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-11 col-md-offset-0">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Registrar estado</h3></div>
            <div class="panel-body">
                <!--COMTENIDO-->
                {!!Form::open(['route'=>'estado.store', 'method'=>'POST'])!!}
                <div class="form-group">
                    {!!Form::label('lbl_detalle','Detalle:')!!}
                    {!!Form::text('detalle',null,['class'=>'form-control','placeholder'=>'Ingresa el valor.'])!!}
                </div>
                {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endsection