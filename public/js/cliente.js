function ciValido(ci){
	if (ci == '' && !isNaN(ci)){
		//mostramos mensaje
		return false;
	}
	return true;

}

function buscarCliente(){
	ci=$('#ci').val();
	token=$('#token').val();
	//	window.alert('algo paso'+ci);
//validar que ci no este vacio mandar mensaje 
    if(ciValido(ci)){
    		$.ajax({
		url: 'http://localhost/diplomando/proyecto/public/buscli',
		headers: {'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data:{ci:ci},
		success:function(resp){
			if (resp.nombComple != ''){
				//window.alert('algo paso'+resp.servicios);										

				$('#nombreCompleto').val(resp.nombComple);	
				$("#servicio option ").remove();
				$(resp.servicios).each(function(key, value){					
					//window.alert('algo paso'+value.nombre);	
					$("#servicio").append('<option value="'+value.id+'" >'+value.nombre+'</option>');				
				});	
				$("#servicio option:first").attr('selected','selected');			
			}else{
				window.alert('error');										
			}
			
		},
	});


    }else{
    	//resetar ci y mandar mensaje 
		window.alert('introdusca ci valido');
    }
//bloquear para que ci cea solo numerico


}

function agregarServicio(){
	ci=$('#ci').val();
	idServi=$('#servicio').val();
	cantidad=$('#cantidad').val();

	//verificar que id servicio no este vacio y que ci cliente no este vacio mandar mensaje

	//window.alert('msj'+idServi+cantidad);
	$.ajax({
		url: 'http://localhost/diplomando/proyecto/public/itemservi',
		headers: {'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data:{idServi:idServi,cantidad:cantidad,ci:ci},
		success:function(resp){
			    $("#tabla-nota tbody > tr").remove();
			    filas='';
				$(resp.arrayServi).each(function(key, value){	
					filas+='<tr>';
					filas+='<td>'+value.idservi+'</td>';
					filas+='<td>'+value.nombreservi+'</td>';
					filas+='<td>'+value.cantidad+'</td>';
					filas+='<td>'+value.precio+'</td>';
					filas+='</tr>';	

					//window.alert('item'+value.nombreservi);				
				});	
				filas+='<td colspan="4" style=" text-align:right;">Total:$'+resp.total+'</td>';
					
				$('#tabla-nota').append(filas);
		},
	});

}

function quitarServicio(){
	idservi=1;
		$.ajax({
		url: 'http://localhost/diplomando/proyecto/public/quitarservi',
		headers: {'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data:{idServi:idservi},
		success:function(resp){
			limpiarCampos();			
		},
	});


}

function guardarNotaVenta(){
	accion='save';
	$.ajax({
		url: 'http://localhost/diplomando/proyecto/public/guardarnota',
		headers: {'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data:{accion:accion},
		success:function(resp){

				//window.alert(resp.fecha);
				//fecha
				fecha='Fecha :'+resp.fecha;
				$( "#fechaD" ).empty();
				$( "#fechaD" ).text(fecha);
				//nombre usuario 
				nombreComp='Usuario :'+resp.cliente.nombres+' '+resp.cliente.apellidos;
				$( "#userD" ).empty();
				$( "#userD" ).text(nombreComp);
				//ci
				ci='Nit  :'+resp.cliente.ci;
				$( "#nitD" ).empty();
				$( "#nitD" ).text(ci);

				//detalle
			    $("#tabla-detalle tbody > tr").remove();
			    filas='';
				$(resp.arrayServi).each(function(key, value){
					//window.alert('dd'+value.nombreservi);	
					filas+='<tr>';			
					filas+='<td>'+value.nombreservi+'</td>';					
					filas+='<td>'+value.precio+'</td>';
					filas+='</tr>';	

					//window.alert('item'+value.nombreservi);				
				});	
				filas+='<td colspan="2" style=" text-align:right;">Total:$'+resp.total+'</td>';
					
				$('#tabla-detalle').append(filas);			
				mostrarDetalle();
				limpiarCampos();

				//window.alert('item se gurado');				

		},
	});
	
}

function mostrarDetalle(){
	//otener todo el detalle de la nota setear en el modal y mostrar
	$('#modalDetalle').modal('show');

}



function limpiarCampos(){
	   $("#tabla-nota tbody > tr").remove();
	   $("#servicio option ").remove();
	   $( "#ci" ).val('');
	   $( "#nombreCompleto" ).val('');


}


//crear metodo para cancelar nota venta
function cancelar(){}
//crear metodo para cancelar item nota venta
function cancelarItem(){} 
